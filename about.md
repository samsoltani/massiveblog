---
layout: page
title: Jekyll Theme - About - Massively
description: When building a website it's helpful to see what the focus of your site is. This page is an example of how to show a website's focus.
sitemap:
    priority: 0.7
    lastmod: 2017-11-02
    changefreq: weekly
---
## About Humancoder

<span class="image left"><img src="{{ "/images/pic04.jpg" | absolute_url }}" alt="" /></span>
### "If you soak and tan your mind into the vats of literature, you can not help but lift your own sight." 
-Dale Carnegie-


This blog might not hit its mark. or it might not be direct enough or maybe too direct.
I try to get out my comfort zone writing it. I am hoping you will get out of yours to read it. I hope you enjoy it.

<div class="box">
  <p>
  Looking at things from a different angle. Providing inspiring stories from success gurus we can learn from.
  </p>
</div>

<!-- <span class="image left"><img src="{{ "/images/pic05.jpg" | absolute_url }}" alt="" /></span> -->

I am very glad to announce the start of our blog here at Humancoders. Drop me a couple of lines. I am open to new ideas and input that we might use in the future to make the blog a better one.
