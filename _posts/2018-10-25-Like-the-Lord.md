---
layout: post
title:  "Be Like the Lord"
date:   2018-10-04
excerpt: "How getting good advice helps us thrive"
image: "/images/pic-07.jpg"
---

## "A shortcut to success; Have good people around you"
-Baltasar Gracian-


Reality shows are not always that real. But I find the Apprentice UK an amazing reality show and competition in which one can learn a lot from the master; Lord Sugar.

In his master class, Lord Sugar does the decision making always with the help of good experts around him. Being himself in Tech industry for years, he is shown always to take advice from body of industry expert whenever he wants to make a business decisions.

With professional story telling techniques, The Apprentice shows how different opinions are summarized to him and points are made about candidates. 

… this story continues. by -- Sam Soltani --