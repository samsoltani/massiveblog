---
layout: post
title:  "Asking Interesting Questions!"
date:   2018-10-03
excerpt: "How looking at Data differently results better outcomes"
image: "/images/pic-10.jpg"
---

## "We have to change our paradigms. We have to change the lens we see the world."


In today’s competitive market, staying in business is hard enough, let alone to being at the top. Things are getting out of control and selling rates are dropping and some companies although big are struggling. Few executives know what’s at stake and can turn things around. Some companies appoint executives and professionals who think outside of the box and see the big picture. Some others don’t. They believe that People with right answers are the key. But how can we have the right answer if do not ask the right questions?
Why some companies are being great and some are merely good?

Sam Walton in his book “Made in America” wrote that “if you buy a good expensive, you’re buying someone else’s inefficiency.” and some where else in his book he mentions about Walmart that “People were ready to drive miles to buy their items cheaper.” Nowadays thanks to point and bonus cards, retailers are capable technically to have access to customer information and their origin to the destination which is their branches. Then they can ask “interesting Questions” and get interesting and useful results.

Great tools like Python and R enable companies and data scientists to better look at their data and make better business decisions by asking “Interesting Questions” first.

In his outstanding book “The Psychology of Selling” Brian Tracy narrates: “I got into sells and my earning was from commissions and I could not sell well. Then I asked a seller in our firm that what selling very well and earning excellent commissions. I went to the salesman and asked him what he does in selling and make him a successful seller?” 

Even to his surprise, his colleague shared with him what he usually did in sales, enabling him to close so many sales.  Definitely did Brian ask an Interesting Question. A Question which change his life. by -- Sam Soltani --