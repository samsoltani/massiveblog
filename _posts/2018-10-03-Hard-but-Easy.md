---
layout: post
title:  "Hard but Easy!"
date:   2018-10-04
excerpt: "How preparation can change our life"
image: "/images/pic-09.jpg"
---

## "Selling is easy if you work it hard, and it is hard when we work it easy."


You might have heard the expression especially from sales gurus that selling is easy if you work it hard and it is hard if you work it easy. But isn’t everything? 

Preparing for a mid-term exam and long nights and early mornings are only one side of the story. If an internship follows and there was no preparation, everyday at work will be a nightmare. Short after, comes the final examination(s) and not only going to exam center feels terrible, the whole experience reminds us of Einstein example for relativity and sitting on a hot cooker. With or without repetition, one looks for a job and come demanding interviews, for programmers the tests and code-tests and even with passing this challenging phase, come the days at work with projects thrown at her.

… this story continues. by -- Sam Soltani --