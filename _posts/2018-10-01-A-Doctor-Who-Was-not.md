---
layout: post
title:  "A Doctor who wasn't!"
date:   2018-10-01
excerpt: "A Moving Story of a Data Scientist"
image: "/images/pic-11.jpg"
comments: true
---

## "Be the Best at what you do and the world will be the path to your door."


Born to Persian parents in England, Leila , 18, attended University to pursue medical degree and to be doctor. Her father, an engineer, insisted that she study medicine and be a doctor. Realising that medicine is not for her, and a big disappointment to her father, she dropped college to study computer science.

Not long after graduation, she knew she had to gain some real-world experience. What impressed her future employer was that Leila called the CEO directly and showed enthusiasm and offered her services without expecting to be paid top salary and demanding to work and learn from company’s Data Scientists. Not surprisingly, she was offered a junior position in the company.

Not long after, and now competent in her craft, she received an email from someone asking if she was interested to work for England Premier League of Football. She accepted and after a couple of interviews and taking some Data Analysis tests, she was accepted.

Today, she is working as a Data Scientist in one of the top football leagues in the World and as Doug Rose, Data Scientist guru says, asks  interesting empirical questions. -- by Sam Soltani --

(Name(s) from our true stories are used in a fictitious manner)