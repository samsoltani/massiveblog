---
layout: post
title:  "First Stop the Leaks"
date:   2018-10-04
excerpt: "How getting good advice helps us thrive"
image: "/images/pic-06.jpg"
---

## "Touch not the tenth that is fattening the purse"



I had a friend who grew up in a not so well family. His parents were smokers, addicts and as a result were not into good social status and work either. "That wasn't all to our misfortune." he added bitterly; "From time to time they would decide to quit consuming those substances and there was a doctor in our neighborhood that took advantage of such people ignorance and by mixing some painkillers, used to sell capsules to them at very high prices to help them quit. That would cost us alot of money and eat up what we had for everything." He narrated.

### "I knew that this wasn't the life I would want when I grow up"

After a few year and he was still in his teens, his mother passed away and he decided to rent a room and find a part-time job to pay the rent. He signed up for English classes and chose the best in the city. He believed that the education system was negatively influenced and in order to really get the best education, he had to get it from places that were unbiased.

He saved up some money and bought himself a desktop computer. Then he started to teach himself Office packages and basic things like email and internet technologies. 

He then found some part time jobs in which he could apply his newly learned language skills.

### "Skills like touch typing were not the most exciting to learn, but I knew it would stay with me like forever."

### "Things like smoking and alcohol are leaks in your life and prevents you from saving up and manage your finances."

Soon later, he received a part time offer at a company which he could now apply his computer skills. He made success happen when and where there was none. Today he is also a successful computer expert working at an enterprise. He was born in darkness but molded by it. He is truly a real example of lasting achievement.


 -- Sam Soltani --